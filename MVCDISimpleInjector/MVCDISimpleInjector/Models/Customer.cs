﻿using System.Security.Cryptography.X509Certificates;

namespace MVCDISimpleInjector.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}